# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from .common.utils import register_classes, unregister_classes

class Render_Tools_Panel(bpy.types.Panel):
    """Creates a Render Panel in the Object properties window"""
    bl_label = "Render Tools"
    bl_idname = "OBJECT_PT_render_tools_panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"
    

    def draw(self, context):
        row = self.layout.row()
        half = row.split(factor=0.6, align=True)
        half.prop(context.scene, "expo_file_location", text="")
        half.operator("anim.export_exposures", text="Export exposures")
        
        row = self.layout.row()
        row.operator("anim.add_empty_tracker", text="Create tracker")
        row = self.layout.row()
        op=row.operator("anim.switch_use_lights", text="Switch off use_lights")
        row.prop(context.scene, "use_lights_switch_selected_only", text="Use selected only")       
        op.selected_only = context.scene.use_lights_switch_selected_only
        
        


def register() :
    bpy.utils.register_class(Render_Tools_Panel)
    bpy.types.Scene.expo_file_location = bpy.props.StringProperty(name="Exposure export location")
    bpy.types.Scene.use_lights_switch_selected_only = bpy.props.BoolProperty(name="Use selected only", default=True)

def unregister() :
    del bpy.types.Scene.use_lights_switch_selected_only 
    del bpy.types.Scene.expo_file_location
    bpy.utils.unregister_class(Render_Tools_Panel)