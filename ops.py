# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import os
from .common.utils import register_classes, unregister_classes
from .common import gp_utils

from . import core


class AAT_OT_export_exposures(bpy.types.Operator) :
    bl_idname = "anim.export_exposures"
    bl_label = "Export exposure frames"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        scene = context.scene        

        frames = core.get_selection_exposure_frames()
        core.export_frames(frames, scene.expo_file_location)

        print("Exporting exposures to %s !"%scene.expo_file_location)
        return {'FINISHED'}

class AAT_OT_add_empty_tracker(bpy.types.Operator) :
    bl_idname = "anim.add_empty_tracker"
    bl_label = "Add an empty tracking the motion of selection."
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def invoke(self, context, event):
        wm = context.window_manager
        self.name = "CTRL_"
        return wm.invoke_props_dialog(self, width=150)
    
    def draw(self, context):
        layout = self.layout
        layout.prop(self, "name", text="Name : ")

    def execute(self, context):
        collection = context.scene.collection
        if "COL-RDR" in bpy.data.collections.keys() :
            collection = bpy.data.collections["COL-RDR"]


        empty = bpy.data.objects.new(self.name, None) 
        collection.objects.link(empty)  
        empty.empty_display_type = 'PLAIN_AXES'

        core.animate_tracker(empty)
        return {'FINISHED'}
    
class AAT_OT_switch_use_lights(bpy.types.Operator) :
    '''Switch off use lights aff all the layers of GP Objects'''
    bl_idname = "anim.switch_use_lights"
    bl_label = "Switch use lights"
    bl_options = {'REGISTER', 'UNDO'}
    selected_only : bpy.props.BoolProperty(default=True)

    def invoke(self, context, event):
        return self.execute(context)
    
    def execute(self, context):
        scene = context.scene
        if not self.selected_only :
            object_list =  gp_utils.get_all_gp_objects()
        else :
            #selected obects only
            object_list = context.selected_objects
        layers = []
        for o in object_list :
            if o.type == 'GPENCIL':
                layers += o.data.layers    
        for layer in layers :
            layer.use_lights = False
        return {'FINISHED'}
    
classes = [
        AAT_OT_export_exposures,
        AAT_OT_add_empty_tracker,
        AAT_OT_switch_use_lights,
    ]

def register() :
    register_classes(classes)

def unregister() :
    unregister_classes(classes)