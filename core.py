# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np
import shapely
import mathutils

from .common.utils import register_classes, unregister_classes
from .common import gp_utils, geometry_3D as geo3D, log_3D, blender_ui


def get_selection_exposure_frames() :
    frame_list = []
    object_names = []

    for o in bpy.context.selected_objects :
        if isinstance(o.data, bpy.types.GreasePencil) and not o.hide_viewport:
            object_names.append(o.name)
            o_frames = get_exposure_frames(o)
            t_frames = get_transform_frames(o)
            # print("Transform frames : ", t_frames)
            frame_list += o_frames + t_frames

    frame_list = list(set(frame_list))
    frame_list.sort()
    
    # print("Exporting frames for ", ", ".join(object_names))
    # print("Frames : ", frame_list)

    return frame_list

def get_exposure_frames(gp_obj) :
    frame_list = []

    for layer in gp_obj.data.layers :
        if not layer.hide :
            layer_frames = [f.frame_number for f in layer.frames]
            frame_list += layer_frames
    
    frame_list = list(set(frame_list))
    frame_list.sort()
    return frame_list


def get_transform_frames(gp_obj) :
    frame_list = []

    if gp_obj.animation_data is None :
        return []

    action = gp_obj.animation_data.action
    for fcurve in action.fcurves :
        frame_list += [int(f.co[0]) for f in fcurve.keyframe_points]
    
    frame_list = list(set(frame_list))
    frame_list.sort()
    return frame_list
    

def export_frames(exposure_frames, path) :
    with open(path, "w") as f :
        f.write(", ".join([str(f) for f in exposure_frames]))


def get_poly(geo_list) :
    polys = []

    for geo in geo_list :
        geo = [(p[0], p[2]) for p in geo]
        poly = shapely.Polygon(geo)
        poly = poly.simplify(0.001)
        poly = shapely.make_valid(poly)
        polys.append(poly)

    return shapely.union_all(polys)


def get_depth(geo_list) :
    full_geo = []

    for geo in geo_list :
        full_geo += geo
    
    return np.mean(np.array(full_geo)[:,1], axis=0)


def geo(stroke, obj) :
    return [obj.matrix_world @ p.co for p in stroke.points]

def get_mask_layer_names(obj) :
    mask_layer_names = []

    for l in obj.data.layers :
        if l.use_mask_layer : 
            mask_layer_names += [m.name for m in l.mask_layers]
            
    return list(set(mask_layer_names))


def get_center_of_mass(objects) :

    # print("Calculating center of mass for ", [o.name for o in objects])

    centroid_list = []
    area_list = []

    for obj in objects :
        mask_names = get_mask_layer_names(obj)
        # print("Masks for %s : "%obj.name, mask_names)
        for l in obj.data.layers :
            if l.active_frame is None or l.hide or l.info in mask_names :
                continue

            geo_list = [geo(s, obj) for s in l.active_frame.strokes]
            geo_list = [g for g in geo_list if len(g) > 5]

            if len(geo_list) == 0 :
                continue

            poly = get_poly(geo_list)
            depth = get_depth(geo_list)
            area = shapely.area(poly)
            centroid = shapely.centroid(poly)
            centroid_3D = np.array([centroid.x, depth, centroid.y])

            # log_3D.point(centroid_3D, color_name="YELLOW")

            # print("\n%s.%s : "%(obj.name, l.info))
            # print("  Area : ", area)    
            # print("  Depth : ", depth)
            # print("  3D Centroid : ", centroid_3D)

            centroid_list.append(centroid_3D)
            area_list.append(area)
    
    if len(centroid_list) == 0 :
        return None

    total_area = np.sum(np.array(area_list))
    weighted_centroids = np.array([(area_list[i]/total_area)*centroid_list[i] for i in range(0, len(centroid_list))])
    final_centroid = np.sum(weighted_centroids, axis=0)

    # print("Final centroid : ", type(final_centroid.tolist()), final_centroid.tolist())
    return mathutils.Vector(final_centroid.tolist())


def animate_tracker(tracker_object) :
    frames = get_selection_exposure_frames()
    r3d = bpy.context.space_data.region_3d
    scene = bpy.context.scene

    selection = bpy.context.selected_objects

    for f in frames :
        scene.frame_set(f)
        bpy.context.view_layer.update()

        center_of_mass = get_center_of_mass(selection)

        if center_of_mass is None :
            continue

        tracker_object.location.x = center_of_mass[0]
        tracker_object.location.y = center_of_mass[1]
        tracker_object.location.z = center_of_mass[2]

        tracker_object.keyframe_insert(data_path='location', frame=f)



classes = []

def register() :
    register_classes(classes)

def unregister() :
    unregister_classes(classes)